module.exports = function(grunt) {

  // Configuration goes here
  grunt.initConfig({          
        
    sass: {
        dist: {          
          files: {              
              'src/temp/all.css': 'src/all.scss'
          }
        }
    },        
    
    concat: {
        js: {
            src: ['bower_components/jquery/dist/jquery.js', 'bower_components/foundation/js/foundation.js'],
            dest: "dest/js/gi.js"
        },
        jsMin: {
            src: ['bower_components/jquery/dist/jquery.min.js', 'bower_components/foundation/js/foundation.min.js'],
            dest: "dest/js/gi.min.js"
        },
        js_ie8: {
            src: ['bower_components/jquery-1.x/dist/jquery.js', 'bower_components/foundation/js/foundation.js'],
            dest: "dest/js/gi-ie8.js"
        },
        jsMin_ie8: {
            src: ['bower_components/jquery-1.x/dist/jquery.min.js', 'bower_components/foundation/js/foundation.min.js'],
            dest: "dest/js/gi-ie8.min.js"
        },
        css: {
            src: ['src/temp/all.css', 'bower_components/foundation-icon-fonts/foundation-icons.css'],
            dest: "dest/css/gi.css"
        }
    },
    
    pixrem: {
        dist:{
          src: "dest/css/gi.css",
          dest: "dest/css/gi-ie8.css"
        }
    },

    cssmin: {
        minify: {
            expand: true,
            cwd: 'dest/css/',
            src: ['*.css', '!*.min.css'],
            dest: 'dest/css/',
            ext: '.min.css'
        }
    },    
    
    uglify: {
        options: {},
        assets: {
            files: {
                'dest/js/modernizr.min.js': ['bower_components/modernizr/modernizr.js']
            }
        }
    },
    
    copy: {
      customFonts: {
        files: [
          {flatten: true, expand: true, src: ['src/fonts/*'], dest: 'dest/fonts/'}
        ]
      },      
      foundationFonts: {
        files: [
          { flatten: true, 
            expand: true, 
            src: [
              'bower_components/foundation-icon-fonts/foundation-icons.eot',
              'bower_components/foundation-icon-fonts/foundation-icons.svg',
              'bower_components/foundation-icon-fonts/foundation-icons.ttf',
              'bower_components/foundation-icon-fonts/foundation-icons.woff'], 
            dest: 'dest/css/'}
        ]
      },
      jsAssets: {
        files: [
          { flatten: true, 
            expand: true, 
            src: [
              'bower_components/respondJs/dest/respond.min.js','bower_components/jquery/dist/jquery.min.map'], 
            dest: 'dest/js/'}
        ]
      }      
    }       
  });

  // Load plugins here
  grunt.loadNpmTasks('grunt-contrib-clean');    
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify'); 
  grunt.loadNpmTasks('grunt-sass');   
  grunt.loadNpmTasks('grunt-pixrem');   

  // Define your tasks here
  grunt.registerTask('default', ['sass', 'concat', 'pixrem', 'cssmin', 'uglify:assets', 'copy:customFonts','copy:foundationFonts', 'copy:jsAssets']);  
  
};